#pragma once

#include <iostream>
#include <string>

class Cell
{
public:
    /**
     * @brief Construct a new Cell object
     * 
     */
    Cell();

    /**
     * @brief Returns the value of the cell.
     * 
     * @return size_t = value
     */
    size_t getCellValue() const;

    /**
     * @brief Sets the new value of the cell.
     * 
     * @param newVal = new value
     */
    void setCellValue(size_t newVal);

private:
    /**
     * @brief The current value of the cell (0=default=empty cell).
     */
    size_t val_;
};