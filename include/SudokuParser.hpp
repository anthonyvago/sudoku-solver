#pragma once

#include "Cell.hpp"

#include <vector>

#include "Utils.hpp"


class Parser
{
public:
    /**
     * @brief Construct a new Parser object
     * 
     */
    Parser();

    /**
     * @brief Parses the given sudoku in the given .txt-file and gives it back as a 2D-vector.
     * 
     * @param argc number of arguments; must be 2 (1=.exe-file=DEFAULT & 2=given sudoku.txt-file)
     * @param argv the arguments itself
     * @return std::vector<std::vector<Cell>> 2d-map of sudoku
     */
    std::vector<std::vector<Cell>> parseSudoku(int argc, char **argv);

private:
    /**
     * @brief This method converts all integer-values in a string to a vector of those integers and returns this vector. 
     * 
     * @param str Given string
     * @return std::vector<size_t> vector of integer-values found in the given string. 
     */
    std::vector<size_t> getAllIntegersFromString(const std::string &str);

    const std::vector<size_t> SIZE_POSSIBILITIES = {9, 12, 16, 25, 100};
};