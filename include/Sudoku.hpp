#pragma once

#include "SudokuParser.hpp"
#include "Cell.hpp"

class Sudoku
{
public:
    /**
     * @brief Construct a new Sudoku object
     *
     * @param sudokuMap 2D-map of sudoku
     */
    Sudoku(std::vector<std::vector<Cell>> sudokuMap);

    /**
     * @brief This method solves the sudoku. This method calls itself multiple times during runtime.
     *
     * @return true = sudoku is solved
     * @return false = sudoku is not solvable
     */
    bool solve();

    /**
     * @brief Prints the current 2D-map of the sudoku.
     *
     */
    void printSudoku();

private:
    /**
     * @brief This method returns if the sudoku is completely filled in.
     *
     * @return true if sudoku is finished.
     */
    bool sudokuFinished();

    /**
     * @brief This method checks whether there is an empty cell present in the current 2D-map of the sudoku.
     *
     * @param row This variable is being changed with the row-number if there is indeed an empty cell.
     * @param col This variable is being changed with the column-number if there is indeed an empty cell.
     * @return true if there is an empty cell in the 2D-map of the sudoku.
     * @return false if there is NOT an empty cell in the 2D-map of the sudoku.
     */
    bool findEmptyCell(size_t &row, size_t &col);

    /**
     * @brief This method checks if the given number fits in the given space (row- & column-number).
     *
     * @param row row-index
     * @param col column-index
     * @param num specific number
     * @return true if the number indeed fits into the given space.
     * @return false if the given number DOES NOT fit into the given space.
     */
    bool isValidPlace(size_t row, size_t col, size_t num);

    /**
     * @brief
     *
     * @param value
     * @param boxStartRow
     * @param boxStartCol
     * @return true
     * @return false
     */
    bool valuePresentInBox(size_t value, size_t boxStartRow, size_t boxStartCol);

    bool valuePresentInRow(size_t value, size_t rowNr);

    bool valuePresentInColumn(size_t value, size_t colNr);

    /**
     * @brief This method initialises the 'boxWidth_' and 'boxHeight_' variables based on the total size of the sudoku.
     *
     */
    void updateBoxSizes();

    /**
     * @brief This variable represents the 2D-map of the sudoku with Cells in it which contain the value of the cell (gets
     * initialised in constructor).
     *
     */
    std::vector<std::vector<Cell>> map_;  // 2D-map

    /**
     * @brief Size of the sudoku (gets initialised in constructor)
     *
     */
    const size_t SUDOKU_SIZE;

    /**
     * @brief Width of one box in the sudoku (gets initialised in constructor); cannot be CONST because this variable
     * depends on the total size of the sudoku and this value differs).
     *
     */
    size_t boxWidth_;

    /**
     * @brief Height of one box in the sudoku (gets initialised in constructor); cannot be CONST because this variable
     * depends on the total size of the sudoku and this value differs).
     *
     */
    size_t boxHeight_;
};