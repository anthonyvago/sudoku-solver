#include "Cell.hpp"

using namespace std;

Cell::Cell() : val_(0)
{
    // cout << __PRETTY_FUNCTION__ << "\n";
}

size_t Cell::getCellValue() const
{
    return val_;
}

void Cell::setCellValue(size_t newVal)
{
    val_ = newVal;
}