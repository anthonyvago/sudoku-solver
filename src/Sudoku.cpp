#include "Sudoku.hpp"

#include <algorithm>
#include <utility>

using namespace std;

Sudoku::Sudoku(std::vector<std::vector<Cell>> sudokuMap) : map_(std::move(sudokuMap)), SUDOKU_SIZE(map_.size()), boxWidth_(0), boxHeight_(0)
{
    // cout << __PRETTY_FUNCTION__ << "\n";
    updateBoxSizes();
}

bool Sudoku::sudokuFinished()
{
    for (auto &row: map_) {
        for (auto &col: row) {
	  if (col.getCellValue() == 0)
	      return false;
        }
    }
    return true;
}

void Sudoku::updateBoxSizes()
{
    switch (SUDOKU_SIZE) {
        case 9:
	  boxWidth_ = 3;
	  boxHeight_ = 3;
	  break;
        case 12:
	  boxWidth_ = 4;
	  boxHeight_ = 3;
	  break;
        case 16:
	  boxWidth_ = 4;
	  boxHeight_ = 4;
	  break;
        case 25:
	  boxWidth_ = 5;
	  boxHeight_ = 5;
	  break;
        case 100:
	  boxWidth_ = 10;
	  boxHeight_ = 10;
	  break;
    }
}

bool Sudoku::solve()
{
    while (!sudokuFinished()) {
        for (size_t row = 0; row < map_.size(); row++) {
	  for (size_t col = 0; col < map_[row].size(); col++) {
	      if (map_[row][col].getCellValue() == 0) {
		vector<size_t> possibilities;
		for (size_t num = 1; num <= SUDOKU_SIZE; num++) {
		    if (isValidPlace(row, col, num)) {
		        possibilities.push_back(num);
		    }
		}
		// If only 1 possibility:
		if (possibilities.size() == 1) {
		    map_[row][col].setCellValue(possibilities[0]);
		}
	      }
	  }
        }
    }
    return true;
}

bool Sudoku::findEmptyCell(size_t &row, size_t &col)
{
    for (row = 0; row < SUDOKU_SIZE; row++) {
        for (col = 0; col < SUDOKU_SIZE; col++) {
	  if (map_[row][col].getCellValue() == 0) {
	      return true;
	  }
        }
    }
    return false;
}

bool Sudoku::isValidPlace(size_t row, size_t col, size_t num)
{
    return !valuePresentInRow(num, row) && !valuePresentInColumn(num, col) &&
	 !valuePresentInBox(num, row - (row % boxHeight_), col - (col % boxWidth_));
}

bool Sudoku::valuePresentInBox(size_t value, size_t boxStartRow, size_t boxStartCol)
{
    for (size_t row = 0; row < boxHeight_; row++) {
        for (size_t col = 0; col < boxWidth_; col++) {
	  if (map_[row + boxStartRow][col + boxStartCol].getCellValue() == value) {
	      return true;
	  }
        }
    }
    return false;
}

bool Sudoku::valuePresentInRow(size_t value, size_t rowNr)
{
    for (auto & i : map_[rowNr]) {
        if (i.getCellValue() == value) {
	  return true;
        }
    }
    return false;
}

bool Sudoku::valuePresentInColumn(size_t value, size_t colNr)
{
    for (auto & i : map_) {
        if (i[colNr].getCellValue() == value) {
	  return true;
        }
    }
    return false;
}

void Sudoku::printSudoku()
{
    // ! Following is to print the final sudoku:
    for (size_t i = 0; i < map_.size(); i++) {
        for (size_t j = 0; j < map_[i].size(); j++) {
	  cout << map_[i][j].getCellValue() << " ";
	  if ((j + 1) % boxWidth_ == 0 && j + 1 != SUDOKU_SIZE) {
	      cout << "\t|\t";
	  }
        }
        cout << "\n";
        if ((i + 1) % boxHeight_ == 0 && i + 1 != SUDOKU_SIZE) {
	  // For printing the correct amount of dashes:
	  const float DASHES_PER_CELL = 5.2f;
	  size_t totalNrOfDashes = size_t(DASHES_PER_CELL * (float) SUDOKU_SIZE);
	  for (size_t dashNr = 0; dashNr < totalNrOfDashes; dashNr++) {
	      cout << "-";
	  }
	  cout << "\n";
        }
    }
}
