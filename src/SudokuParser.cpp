#include "SudokuParser.hpp"

#include <fstream>
#include <sstream>
#include <algorithm>

using namespace std;

Parser::Parser()
{
    // cout << __PRETTY_FUNCTION__ << "\n";
}

vector<vector<Cell>> Parser::parseSudoku(int argc, char **argv)
{
    if (argc != 2)
        throwException(
	      "Unexpected number of arguments given to program; give only one argument which is the sudoku.txt-file in the "
	      "root-folder!");

    vector<vector<Cell>> newMap;

    string line;
    string filename = "../" + string(argv[1]);

    ifstream file(filename);
    if (file.is_open()) {
        getline(file, line, 'x');
        uint8_t nRows = (uint8_t) stoi(line);

        getline(file, line);
        uint8_t nCols = (uint8_t) stoi(line);

        // Check if configuration was correct:
        if (nCols != nRows)
	  throwException("Number of rows and columns need to be the same!");

        // It does not matter whether I pick 'nCols' or 'nRows' in the following lines of code because at this point at
        // runtime they are both the same.
        // Check if the size is a correct one:
        auto it = find(SIZE_POSSIBILITIES.begin(), SIZE_POSSIBILITIES.end(), nCols);
        if (it == SIZE_POSSIBILITIES.end())
	  throwException("Number of rows and columns can only be: 9x9, 12x12, 16x16, 25x25 or 100x100!");

        // Go through each row:
        newMap.resize(nRows);
        for (uint8_t rowIndex = 0; rowIndex < nRows; rowIndex++) {
	  newMap[rowIndex].resize(nCols);

	  getline(file, line);

	  if (line == "")
	      throwException("Number of filled in rows does not match the specified number of rows above!");

	  // Line contains something (can be ideally a number or a letter or a sentence), so we have to check that first:
	  vector<size_t> allIntsInRow = getAllIntegersFromString(line);

	  // Check if the row has the same amount of columns as specified:
	  if (nCols != (uint8_t) allIntsInRow.size())
	      throwException("In row number " + to_string(rowIndex + 1) +
			 " of the sudoku-map there are more/less numbers so more/less columns than specified above!");

	  // Set cell value to the parsed number:
	  for (uint8_t colIndex = 0; colIndex < nCols; colIndex++) {
	      newMap[rowIndex][colIndex].setCellValue(allIntsInRow[colIndex]);
	  }
        }

        file.close();
    } else {
        throwException("Could not open file: \"" + string(argv[1]) + "\"; could be missing or misplaced!");
    }

    return newMap;
}

vector<size_t> Parser::getAllIntegersFromString(const string &str)
{
    vector<size_t> foundIntegerVec;

    stringstream ss;

    ss << str;

    string temp;
    size_t found;
    while (!ss.eof()) {
        ss >> temp;

        if (stringstream(temp) >> found) {
	  foundIntegerVec.push_back(found);
        }

        temp = "";
    }

    return foundIntegerVec;
}