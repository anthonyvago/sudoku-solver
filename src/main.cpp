#include <iostream>

using namespace std;

#include "SudokuParser.hpp"
#include "Sudoku.hpp"

int main(int argc, char **argv)
{
    cout << "\n\033[1;32mSTART\033[0m\n\n";

    Parser parser;

    try {
        vector<vector<Cell>> map_ = parser.parseSudoku(argc, argv);
        Sudoku sudoku(map_);
        if (sudoku.solve())
	  sudoku.printSudoku();
        else
	  throwException("Could not solve this sudoku; give other sudoku as input in sudoku.txt!");
    }
    catch (const string &e) {
        cout << "\033[1;31mERROR: " << e << "\033[0m\n";
    }

    cout << "\n\033[1;32mEND\033[0m\n\n";

    return 0;
}